angular.module("TahrerApp").factory('angularCachingService', function()
{
    /*
     * User setter & getters
     */
     var loggedUser = {};

     function setUser(data)
     {
        loggedUser = data;
    }

    function getUser()
    {
        return loggedUser;
    }
     return {
        setUser: setUser,
        getUser: getUser,
    }
});
angular.module("TahrerApp").factory('storageService', function($rootScope)
{
    return {
        get: function(key)
        {
            return JSON.parse(localStorage.getItem(key));
        },
        save: function(key, data)
        {
            localStorage.setItem(key, JSON.stringify(data));
        },
        remove: function(key)
        {
            localStorage.removeItem(key);
        },
        clearAll: function()
        {
            localStorage.clear();
        }
    };
});
angular.module("TahrerApp").factory('localStorageService', function($http, storageService)
{
    return {
        getData: function(key)
        {
            return storageService.get(key);
        },
        setData: function(key, data)
        {
            storageService.save(key, data);
        },
        removeData: function(key)
        {
            storageService.remove(key);
        },
        clearAll: function()
        {
            storageService.clearAll();
        }
    };
});

angular.module('TahrerApp')
  .filter('range', function(){
    return function(n) {
      var res = [];
      for (var i = 0; i < n; i++) {
        res.push(i);
      }
      return res;
    };
  });