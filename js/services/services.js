angular.module('TahrerApp').service('Cities', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';
        
        this.listCities = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/cities/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllCities = function()
        {
            return $http.get(serverip + "tahrir/cities/list/name->"+ head.locale +"/"+ 0,
            {
                headers: head
            });
        };
        this.listHomeCities = function(count) 
        {
            return $http.get(serverip + "tahrir/cities/home/"+ count,
            {
                headers : head
            });
        };
        this.getCityByID = function(cityID) 
        {
            return $http.get(serverip + "tahrir/cities/find/" + cityID,
            {
                headers : head
            });
        }; 
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/cities/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };

    }
    ]);
angular.module('TahrerApp').service('Countries', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listCountries = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/countries/paginate/" + items +  '/name->en/1' + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllCountries = function()
        {
            return $http.get(serverip + "tahrir/countries/list/name->"+ head.locale +"/"+ 0,
            {
                headers: head
            });
        };
        this.listCitiesByCountry = function(countryID) 
        {
            return $http.post(serverip + "tahrir/cities/findby/name->"+ head.locale +"/"+ 0,
            {
                country_id:countryID
            },
            {
                headers : head
            });
        };
        this.getCountryByID = function(countryID) 
        {
            return $http.get(serverip + "tahrir/countries/find/" + countryID,
            {
                headers : head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/countries/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Categories', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listCategories = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/categories/paginate/" + items  +  '/order/0' + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllCategories = function()
        {
            return $http.get(serverip + "tahrir/categories/list/name->"+ head.locale +"/"+ 0,
            {
                headers: head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/categories/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Tags', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listTags = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/tags/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllTags = function()
        {
            return $http.get(serverip + "tahrir/tags/list",
            {
                headers: head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/tags/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Partners', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listPartners = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/partners/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllPartners = function()
        {
            return $http.get(serverip + "tahrir/partners/list",
            {
                headers: head
            });
        };
        this.getShownPartners = function()
        {
            return $http.post(serverip + "tahrir/partners/findby",
            {
                hide:0
            },
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('HomeTabs', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listTabs = function()
        {
            return $http.get(serverip + "tahrir/home/tabs/list",
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Sliders', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listSliders = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/slides/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllSliders = function()
        {
            return $http.get(serverip + "tahrir/slides/list",
            {
                headers: head
            });
        };
        this.getShownSliders = function()
        {
            return $http.post(serverip + "tahrir/slides/findby",
            {
                hide:0
            },
            {
                headers: head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/slides/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Rooms', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listRooms = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/rooms/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllRooms = function()
        {
            return $http.get(serverip + "tahrir/rooms/list",
            {
                headers: head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/rooms/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.searchQuick = function(searchText)
        {
            return $http.get(serverip + "tahrir/rooms/search/" + searchText,
            {
                headers: head
            });
        };
        this.roomsByHotelID = function(val, hotelID) 
        {
            var condition = {};
            condition.and = 
            {
                or:
                {
                    "type->en":{"op":"like", "val":"%"+val+"%"},
                    "type->fr":{"op":"like", "val":"%"+val+"%"},
                    "type->sp":{"op":"like", "val":"%"+val+"%"}
                },
                hotels:
                {
                    "op":"has",
                    "val": {"hotels.id":hotelID}
                }
            }
            return $http.post(serverip + "tahrir/rooms/findby", condition,
            {
                headers : head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Hotels', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listHotels = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/hotels/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllHotels = function()
        {
            return $http.get(serverip + "tahrir/hotels/list",
            {
                headers: head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/hotels/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.searchQuick = function(searchText)
        {
            return $http.get(serverip + "tahrir/hotels/search/" + searchText,
            {
                headers: head
            });
        };
        
    }
    ]);
angular.module('TahrerApp').service('Trips', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listTripByID = function(ID) 
        {
            return $http.get(serverip + "tahrir/trips/find/" + ID,
            {
                headers : head
            });
        };

        this.listTrips = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/trips/paginate/" + items +  '/order/0' + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllTrips = function()
        {
            return $http.get(serverip + "tahrir/trips/list",
            {
                headers: head
            });
        };
        this.listFeaturedTripsRandom = function(count)
        {
            count == undefined ? 3 : count;
            return $http.get(serverip + "tahrir/trips/featured/"+ count,
            {
                headers: head
            });
        };
        this.listFeaturedTrips = function(items, page)
        {
            return $http.post(serverip + "tahrir/trips/paginateby/" + items +  '/order/0' +  "?page=" + page,
            {
                featured:1
            },
            {
                headers: head
            });
        };
        
        this.listFilteredTrips = function(items, page, searchText)
        {
            var and = {};

            if(searchText.city_id) 
            {
                and.city_id = searchText.city_id;
            }
            if(searchText.number_of_itineraries) 
            {
                and.number_of_itineraries = searchText.number_of_itineraries;
            }
            if(searchText.categoryIDs) 
            {
                and.categories = 
                {
                    "op": "has",
                    "val": 
                    {
                        "categories.id":
                        {
                            "op": "in",
                            "val": searchText.categoryIDs
                        }
                    }
                }
            }
            // if(searchText.durations) 
            // {
            //     and.number_of_days = 
            //     {
            //         "op": "has",
            //         "val": 
            //         {
            //             "number_of_days":
            //             {
            //                 "op": "in",
            //                 "val": searchText.durations
            //             }
            //         }
            //     }
            // }
            if(searchText.price) 
            {
                if(searchText.price.val1 && searchText.price.val2)
                {
                    and.lowest_price = 
                    {
                        "op": "between",
                        "val1":searchText.price.val1,
                        "val2":searchText.price.val2
                    }                    
                }
                else
                {
                    let val1 = searchText.price.val1.split("+", 1);
                    val1 = val1[0];

                    and.lowest_price = 
                    {
                        "op": ">",
                        "val": val1
                    }
                }

            }
            if(searchText.keyword)
            {
                and.or = {}
                and.or['lower(title->en)'] = {"op":"like", "val":"%"+searchText.keyword.toLowerCase()+"%"};
                and.or['lower(title->fr)'] = {"op":"like", "val":"%"+searchText.keyword.toLowerCase()+"%"};
                and.or['lower(title->sp)'] = {"op":"like", "val":"%"+searchText.keyword.toLowerCase()+"%"};

                and.or['lower(tour_type->en)'] = {"op":"like", "val":"%"+searchText.keyword.toLowerCase()+"%"};
                and.or['lower(tour_type->fr)'] = {"op":"like", "val":"%"+searchText.keyword.toLowerCase()+"%"};
                and.or['lower(tour_type->sp)'] = {"op":"like", "val":"%"+searchText.keyword.toLowerCase()+"%"};

                and.or['tags'] = 
                {                    
                    "op": "has",
                    "val": 
                    {
                        "or": 
                        {
                            "lower(tags.name->en)":
                            {
                                "op":"like",
                                "val":"%"+searchText.keyword.toLowerCase()+"%"
                            },
                            "lower(tags.name->fr)":
                            {
                                "op":"like",
                                "val":"%"+searchText.keyword.toLowerCase()+"%"
                            },
                            "lower(tags.name->sp)":
                            {
                                "op":"like",
                                "val":"%"+searchText.keyword.toLowerCase()+"%"
                            }
                        }
                    }
                }
            }
            if(and.number_of_itineraries || and.city_id || and.or || and.lowest_price || and.categories)
            {
                return $http.post(serverip + "tahrir/trips/paginateby/" + items  +  '/order/0' +  "?page=" + page, 
                {
                    "and":and
                },
                {
                    headers: head
                });
            }
            else
            {
                return $http.get(serverip + "tahrir/trips/paginate/" + items +  '/order/0' + "?page=" + page,
                {
                    headers: head
                }); 
            }
        };

        this.listTripsImage = function(items, page, tripID)
        {
            page == undefined ? 1 : page;
            return $http.post(serverip + "tahrir/trip/images/paginateby/" + items + "?page=" + page,
            {
                trip_id:tripID
            },
            {
                headers: head
            });
        };
        this.reserve = function(data)
        {
            return $http.post(serverip + "tahrir/trip/reservations/reserve", data,
            {
                headers: head
            });
        };
        this.listOfItineraries = function()
        {
            return $http.get(serverip + "tahrir/trips/itineraries",
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Contact', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.contactUs = function(data)
        {
            return $http.post(serverip + "tahrir/contact/send", data,
            {
                headers: head
            });
        };
        this.enquireUs = function(data)
        {
            return $http.post(serverip + "tahrir/contact/enquire", data,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('FAQs', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        if (localStorageService.getData('TahrirFrontlang') === 'us') head.locale = 'en';
        else if (localStorageService.getData('TahrirFrontlang') === 'sp') head.locale = 'sp';
        else if (localStorageService.getData('TahrirFrontlang') === 'fr') head.locale = 'fr';

        this.listFaqs = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/faqs/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllFaqs = function()
        {
            return $http.get(serverip + "tahrir/faqs/list",
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Nationalities', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listNationalities = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/nationalities/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllNationalities = function()
        {
            return $http.get(serverip + "tahrir/nationalities/list",
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('ToursLanguage', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listToursLanguage = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/tour_languages/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllToursLanguage = function()
        {
            return $http.get(serverip + "tahrir/tour_languages/list",
            {
                headers: head
            });
        };
    }
    ]);
angular.module('TahrerApp').service('Settings', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirFrontUser'))
        {
            head.Authorization = localStorageService.getData('TahrirFrontUser').authToken;
        }
        this.listSettings = function()
        {
            return $http.get(serverip + "core/settings/list",
            {
                headers: head
            });
        };

    }
    ]);