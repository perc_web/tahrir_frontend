angular.module("TahrerApp").controller('FooterController', ['$scope', '$rootScope', 'localStorageService', '$state', '$timeout', 'loading_timeout', 'debug','$window','Contact','Settings',
    function ($scope, $rootScope, localStorageService, $state, $timeout, loading_timeout, debug, $window, Contact, Settings) {

    	$scope.contact = {};
    	$scope.contactUS = function()
    	{
            $scope.contactSuccess = false;

    		if ($scope.contactForm.$invalid) 
    		{
    			$scope.contactFormInvalid = true ;
    			return;
    		}

    		$scope.saving  = true;
    		$scope.promise = Contact.contactUs($scope.contact);
    		$scope.promise.then(function()
    		{
    			$scope.contact = {};
                $scope.contactSuccess = true;
    			$scope.saving = false;

                setTimeout(function() 
                {
                    $scope.contactSuccess = false;
                }, 1500);

    		}, function(error)
    		{
                $scope.contactSuccess = false;
    			$scope.saving = false;
    		});
    	};
        

}]);