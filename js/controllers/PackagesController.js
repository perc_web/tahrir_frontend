angular.module("TahrerApp").controller('PackagesController', ['$scope', '$rootScope', 'localStorageService', '$state', '$stateParams', '$timeout', 'loading_timeout', 'debug','$window','Trips', '$filter','Categories','Cities',
  function ($scope, $rootScope, localStorageService, $state, $stateParams, $timeout, loading_timeout, debug, $window, Trips, $filter, Categories, Cities) {

    $scope.items = 12;
    $scope.page  = 1;
    $scope.tripsList = [];
    $scope.filter = {};
    $scope.filter.categoriesIDs = [];
    $scope.priceFilter = 'none';
    $scope.city_id     = 'all';
    $scope.number_of_itineraries     = 'all';

    $scope.PricesList = 
    [
      {"val1":0,"val2":100},
      {"val1":100,"val2":200},
      {"val1":200,"val2":300},
      {"val1":300,"val2":400},
      {"val1":400,"val2":500},
      {"val1":'500+', "val2":""}
    ];
    $scope.getTripDetails = function(trip)
    {
      $state.go('holiday', {tripId:trip.id});
      $rootScope.scrollTop();
    }
    $scope.listOfTrips = function(items, page)
    {
      $scope.dataLoading = true;
      $scope.promise = Trips.listTrips(items, page);
      $scope.promise.then(function(response)
      {
        var data = response.data.data;
        if(data.length)
        {
          $scope.tripsList  = $scope.tripsList.concat(data);
          $scope.pagesCount = response.data.last_page;
          $scope.dataLoading = false;
          $scope.loadMoredata = false;
          $scope.NoData = false;
        } 
        else
        {
          $scope.NoData = true;
          $scope.dataLoading = false;
          $scope.loadMoredata = false;
        }

      }, function(error)
      {
        $scope.dataLoading = false;
        $scope.loadMoredata = false;
        $scope.NoData = false;
      });
    }
    $scope.listOfCategories = function()
    {
      $scope.dataLoading = true;
      $scope.promise = Categories.getAllCategories();
      $scope.promise.then(function(response)
      {
        $scope.categoriesList = response.data;
        $scope.dataLoading    = false;
      }, function(error)
      {
        $scope.dataLoading = false;
      });
    }
    $scope.listOfCategories();

    $scope.listOfCities = function()
    {
      $scope.dataLoading = true;
      $scope.promise = Cities.getAllCities();
      $scope.promise.then(function(response)
      {
        $scope.dataLoading = false;
        $scope.citiesList = response.data;

      }, function(error)
      {
        $scope.dataLoading = false;
      });
    }

    $scope.listOfCities();

    $scope.listOfItineraries = function()
    {
      $scope.dataLoading = true;
      $scope.promise = Trips.listOfItineraries();
      $scope.promise.then(function(response)
      {
        $scope.dataLoading     = false;
        $scope.itinerariesList = response.data;

      }, function(error)
      {
        $scope.dataLoading = false;
      });
    }
    $scope.listOfItineraries();

    $scope.listFilteredTrips = function(items, page, searchText)
    {
      $scope.loadMoredata = true;
      $scope.promise = Trips.listFilteredTrips(items, page, searchText);
      $scope.promise.then(function(response)
      {
        var data = response.data.data;
        if(data.length)
        {
          $scope.tripsList  = $scope.tripsList.concat(data);
          $scope.pagesCount  = response.data.last_page;
          $scope.loadMoredata = false;
          $scope.NoData = false;
        }
        else
        {
          $scope.NoData = true;
          $scope.loadMoredata = false;
        }
      }, function(error)
      {
        $scope.loadMoredata = false;
        $scope.NoData = false;
      });
    }

    $scope.filterPackagesbyCategory = function()
    {
      $scope.page = 1;
      $scope.tripsList = [];
      setTimeout(function() {
        if($rootScope.searchObj && $scope.filter.categoriesIDs.length >= 1)
        {
          $rootScope.searchObj.categoryIDs = $scope.filter.categoriesIDs;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj)
        }
        else if ($rootScope.searchObj && $scope.filter.categoriesIDs.length < 1)
        {
          delete $rootScope.searchObj.categoryIDs;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj) 
        }
        else if( ! $rootScope.searchObj && $scope.filter.categoriesIDs.length >= 1)
        {
          $rootScope.searchObj = {};
          $rootScope.searchObj.categoryIDs = $scope.filter.categoriesIDs;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj) 
        }
        else if( ! $rootScope.searchObj && $scope.filter.categoriesIDs.length < 1)
        {
          delete $rootScope.searchObj;
          $scope.listOfTrips($scope.items, $scope.page);    
        }
      }, 300);
    }
    $scope.filterPackagesbyPrice = function(price)
    {
      $scope.page = 1;
      $scope.tripsList = [];
      if($rootScope.searchObj)
      {
        if(price != "none")
        {
          $rootScope.searchObj.price = price;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj) 
        }
        else
        {
          delete $rootScope.searchObj.price;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj)  
        }
      }
      else
      {
        if(price != "none")
        {
          $rootScope.searchObj = {};
          $rootScope.searchObj.price = price;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj)
        }
        else
        {
          delete $rootScope.searchObj;
          $scope.listOfTrips($scope.items, $scope.page);
        }
      }  
    }
    $scope.filterPackagesbyCity = function(cityID)
    {
      $scope.page = 1;
      $scope.tripsList = [];
      if($rootScope.searchObj)
      {
        if(cityID != "all")
        {
          $rootScope.searchObj.city_id = cityID;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj) 
        }
        else
        {
          delete $rootScope.searchObj.city_id;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj)  
        }
      }
      else
      {
        if(cityID != "all")
        {
          $rootScope.searchObj = {};
          $rootScope.searchObj.city_id = cityID;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj)
        }
        else
        {
          delete $rootScope.searchObj;
          $scope.listOfTrips($scope.items, $scope.page);
        }
      }  
    }
    $scope.filterPackagesbyItineraries = function(number_of_itineraries)
    {
      $scope.page = 1;
      $scope.tripsList = [];
      if($rootScope.searchObj)
      {
        if(number_of_itineraries != "all")
        {
          $rootScope.searchObj.number_of_itineraries = number_of_itineraries;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj) 
        }
        else
        {
          delete $rootScope.searchObj.number_of_itineraries;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj)  
        }
      }
      else
      {
        if(number_of_itineraries != "all")
        {
          $rootScope.searchObj = {};
          $rootScope.searchObj.number_of_itineraries = number_of_itineraries;
          $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj)
        }
        else
        {
          delete $rootScope.searchObj;
          $scope.listOfTrips($scope.items, $scope.page);
        }
      }  
    }
    if($stateParams.categoryId)
    {
      $scope.filter.categoriesIDs.push(parseInt($stateParams.categoryId));
      $scope.filterPackagesbyCategory();
    }
    else if($rootScope.searchObj)
    {
      if($rootScope.searchObj.city_id)
      {
        $scope.city_id = $rootScope.searchObj.city_id;
      }
      if($rootScope.searchObj.number_of_itineraries)
      {
        $scope.number_of_itineraries = $rootScope.searchObj.number_of_itineraries;
      }
        $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj);
    }
    else
    {
      $scope.listOfTrips($scope.items, $scope.page);  
    }
    
    $scope.loadMoreTrips = function()
    {
      $scope.loadMoredata = true;
      $scope.page += 1; 

      if($rootScope.searchObj)
      {
        $scope.listFilteredTrips($scope.items, $scope.page, $rootScope.searchObj) 
      }
      else
      {
        $scope.listOfTrips($scope.items, $scope.page);  
      }
    }
    $scope.searchByCategory = function(categoryID) 
    {
      $scope.filter.categoriesIDs = [];
      $scope.filter.categoriesIDs.push(categoryID);
      $scope.filterPackagesbyCategory();
    }

  }]);