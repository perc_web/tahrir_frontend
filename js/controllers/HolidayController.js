angular.module("TahrerApp").controller("HolidayController", [
  "$scope",
  "$rootScope",
  "localStorageService",
  "$state",
  "$stateParams",
  "$timeout",
  "loading_timeout",
  "debug",
  "$window",
  "Countries",
  "Trips",
  "Cities",
  "Nationalities",
  "ToursLanguage",
  "Contact",
  "$filter",
  function(
    $scope,
    $rootScope,
    localStorageService,
    $state,
    $stateParams,
    $timeout,
    loading_timeout,
    debug,
    $window,
    Countries,
    Trips,
    Cities,
    Nationalities,
    ToursLanguage,
    Contact,
    $filter
  ) {
    (function() {
      $("#holidaySlider").carousel({
        interval: 2000
      });
    })();

    if (!$stateParams.tripId) {
      $state.go("packages");
    }
    $scope.getTripByID = function(id) {
      $scope.dataLoading = true;
      $scope.promise = Trips.listTripByID(id);
      $scope.promise.then(
        function(response) {
          $scope.dataLoading = false;
          console.log(response.data);
          if (response.data.id) {
            $scope.trip = response.data;
            $scope.listCityByID($scope.trip.city_id);
            $scope.getAllNationalities();
            $scope.getAllToursLanguage();
          } else {
            $state.go("packages");
          }
        },
        function(error) {
          $scope.dataLoading = false;
        }
      );
    };
    $scope.listCityByID = function(id) {
      $scope.dataLoading = true;
      $scope.promise = Cities.getCityByID(id);
      $scope.promise.then(
        function(response) {
          $scope.dataLoading = false;
          $scope.trip.city_name = response.data.name;
        },
        function(error) {
          $scope.dataLoading = false;
        }
      );
    };
    $scope.getAllNationalities = function() {
      $scope.dataLoading = true;
      $scope.promise = Nationalities.getAllNationalities();
      $scope.promise.then(
        function(response) {
          $scope.dataLoading = false;
          $scope.nationalitiesList = response.data;
        },
        function(error) {
          $scope.dataLoading = false;
        }
      );
    };
    $scope.getAllToursLanguage = function() {
      $scope.dataLoading = true;
      $scope.promise = ToursLanguage.getAllToursLanguage();
      $scope.promise.then(
        function(response) {
          $scope.dataLoading = false;
          $scope.toursLanguageList = response.data;
        },
        function(error) {
          $scope.dataLoading = false;
        }
      );
    };
    if ($stateParams.tripId) {
      $scope.getTripByID($stateParams.tripId);
    }
    $scope.getCharacter = function(value) {
      return String.fromCharCode(65 + value);
    };
    $scope.getTripDetails = function(trip) {
      $state.go("holiday", {
        tripId: trip.id
      });
      $rootScope.scrollTop();
    };

    $scope.enquire = {};
    $scope.enquire.reservation_details = {};
    $scope.enquire.adults = null;
    $scope.enquire.baby_chair = false;
    $scope.totalSizePrice = [];

    // $scope.calculateTotalPrices = function(priceSize, priceObj, pricesList)
    // {
    // 	$scope.totalSizePrice[priceObj.size] =  priceSize * priceObj.price;
    // 	$scope.totalPrice = 0;
    // 	for(var i = 0; i < pricesList.length; i++)
    // 	{
    // 		if($scope.totalSizePrice[pricesList[i].size])
    // 		{
    // 			$scope.totalPrice += $scope.totalSizePrice[pricesList[i].size] ;
    // 		}
    // 	}
    // 	console.log($scope.totalPrice)
    // }
    $scope.calculateTotalPrices = function(index, priceObj, pricesList) {
      $scope.totalPrice = 0;
      for (var i = 0; i < pricesList.length; i++) 
      {
        if (pricesList[i].from <= $scope.enquire.adults && pricesList[i].to >= $scope.enquire.adults) 
        {
          $scope.totalPrice = $scope.trip.type == "transfer" ? pricesList[i].price : pricesList[i].price * $scope.enquire.adults;
        }
        else if($scope.enquire.adults > pricesList[i].to)
        {
          $scope.totalPrice = pricesList[i].price; 
        }
      }

      if ($scope.trip.type == "trip" || $scope.trip.type == "accommodation") 
      {
        if ($scope.enquire.childs6_numbers) 
        {
          $scope.totalPrice += $scope.trip.childs_2_6 * $scope.enquire.childs6_numbers;
        }
        if ($scope.enquire.childs12_numbers) 
        {
          $scope.totalPrice += $scope.trip.childs_6_12 * $scope.enquire.childs12_numbers;
        }
      } 
      else if ($scope.trip.type == "transfer") 
      {
        $scope.totalPrice += 0;
      } 
      else 
      {
        $scope.totalPrice += 0;
      }
    };
    var mapEnquire = function(enquireData) {
      var mappedData = angular.copy(enquireData);
      delete mappedData.adults;
      delete mappedData.baby_chair;
      delete mappedData.childs6_numbers;
      delete mappedData.childs12_numbers;
      mappedData.number_of_adults = enquireData.adults;
      if ($scope.trip.type == "transfer") {
        mappedData.baby_chair = enquireData.baby_chair;
      } else if ($scope.trip.type == "trip") {
        mappedData.number_of_childs_2_6 = enquireData.childs6_numbers;
        mappedData.number_of_childs_6_12 = enquireData.childs12_numbers;
      }
      return mappedData;
    };
    $scope.enquireUs = function(enquire) {
      enquire = mapEnquire(enquire);
      $scope.enquireSuccess = false;
      if ($scope.enquireForm.$invalid) {
        $scope.enquireFormInvalid = true;
        return;
      }

      enquire.price = $scope.totalPrice ? $scope.totalPrice : 0;
      enquire.trip_id = $scope.trip.id;
      $scope.saving = true;
      $scope.promise = Contact.enquireUs(enquire);
      $scope.promise.then(
        function(response) {
          $scope.enquire = {};
          $scope.enquire.reservation_details = {};
          $scope.totalSizePrice = [];
          $scope.enquire.baby_chair = false;
          $scope.enquireSuccess = true;
          $scope.saving = false;
          setTimeout(function() {
            $scope.enquireSuccess = false;
          }, 1500);
        },
        function(error) {
          $scope.enquireSuccess = false;
          $scope.saving = false;
        }
      );
    };

    $scope.reserve = function(enquire) {
      enquire = mapEnquire(enquire);
      $scope.enquireSuccess = false;
      if ($scope.enquireForm.$invalid) {
        $scope.enquireFormInvalid = true;
        return;
      }

      enquire.price      = $scope.totalPrice ? $scope.totalPrice : 0;
      enquire.price      = Number(enquire.price) + Number(enquire.price * 0.03);
      enquire.price      = enquire.price.toFixed(1);
      enquire.trip_id    = $scope.trip.id;
      enquire.from       = $filter("date")(enquire.from, "yyyy-MM-dd HH:mm:ss");
      enquire.should_pay = $scope.trip.type == "accommodation" ? 0 : 1;
      $scope.saving      = true;
      $scope.promise     = Trips.reserve(enquire);
      $scope.promise.then(
        function(response) {
          $scope.enquire = {};
          $scope.enquire.reservation_details = {};
          $scope.totalSizePrice = [];
          $scope.enquire.baby_chair = false;
          $scope.enquireSuccess = true;
          $scope.saving = false;
          setTimeout(function() {
            $scope.enquireSuccess = false;
          }, 200);
          if ($scope.trip.type !== "accommodation") 
          {
            response.data.trip = $scope.trip;
            localStorageService.setData("holidayData", response.data);
            $state.go("confirmAndPay"); 
          }
        },
        function(error) {
          $scope.enquireSuccess = false;
          $scope.saving = false;
        }
      );
    };
  }
]);
