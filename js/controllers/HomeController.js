angular.module("TahrerApp").controller('HomeController', ['$scope', '$rootScope', 'localStorageService', '$state', '$timeout', 'loading_timeout', 'debug', '$window', 'Countries', 'Trips', 'Sliders', 'Partners', 'Categories','HomeTabs','Cities', '$filter',
  function ($scope, $rootScope, localStorageService, $state, $timeout, loading_timeout, debug, $window, Countries, Trips, Sliders, Partners, Categories, HomeTabs, Cities, $filter) {
    	//setup the slider
      (function(){
       $('#carousel-tilenav').carousel({ interval: 2000 });
       $('#myCarousel').carousel({ interval: 3000 });
     }());
        //Home tabs pictures (Activate Tab)
        $scope.toursImg   = "assets/img/about/tours-blue.png";
        $scope.bookingImg = "assets/img/about/booking-red.png";
        $scope.dealsImg   = "assets/img/about/deals-red.png";
        $scope.activate = function(Tabnum)
        {
          if(Tabnum == 1)
          {
            $scope.bookingImg = "assets/img/about/booking-red.png";
            $scope.dealsImg   = "assets/img/about/deals-red.png";
            $scope.toursImg   = "assets/img/about/tours-blue.png";
          }
          else if (Tabnum == 2)
          {
            $scope.toursImg   = "assets/img/about/tours-red.png";
            $scope.dealsImg   = "assets/img/about/deals-red.png";
            $scope.bookingImg = "assets/img/about/booking-blue.png";
          }
          else if (Tabnum == 3)
          {
            $scope.toursImg   = "assets/img/about/tours-red.png";
            $scope.bookingImg = "assets/img/about/booking-red.png";
            $scope.dealsImg   = "assets/img/about/deals-blue.png";
          }
        }
        $scope.listOfSliders = function()
        {
          $scope.dataLoading = true;
          $scope.promise = Sliders.getShownSliders();
          $scope.promise.then(function(response)
          {
            $scope.dataLoading = false;
            $scope.slidersList = response.data;
          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }
        $scope.listOfSliders();

        $scope.listHomeCities = function(count)
        {
          $scope.dataLoading = true;
          $scope.promise = Cities.listHomeCities(count);
          $scope.promise.then(function(response)
          {
            $scope.dataLoading = false;
            $scope.citiesList = response.data;
          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }
        $scope.listHomeCities(4);

        $scope.listCitiesByCountry = function(countryID)
        {
          $scope.dataTransparentLoading = true;
          $scope.promise = Countries.listCitiesByCountry(countryID);
          $scope.promise.then(function(response)
          {
            $scope.dataTransparentLoading = false;
            $scope.citiesDropdownList = response.data;

          }, function(error)
          {
            $scope.dataTransparentLoading = false;
          });
        }
        
        $scope.listOfCountries = function()
        {
          $scope.dataLoading = true;
          $scope.promise = Countries.getAllCountries();
          $scope.promise.then(function(response)
          {
            $scope.dataLoading = false;
            $scope.country_id  = response.data[0].id;
            $scope.countriesDropdownList = response.data;
            $scope.listCitiesByCountry($scope.country_id)

          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }

        $scope.listOfCountries();

        $scope.listOfCategories = function(items, page)
        {
          $scope.dataLoading = true;
          $scope.promise = Categories.listCategories(items, page);
          $scope.promise.then(function(response)
          {
            $scope.dataLoading = false;
            $scope.categoriesList = response.data.data;
            $scope.pagesCount = response.data.last_page;

          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }

        $scope.listOfCategories(6,1);

        $scope.listOfItineraries = function()
        {
          $scope.dataLoading = true;
          $scope.promise = Trips.listOfItineraries();
          $scope.promise.then(function(response)
          {
            $scope.dataLoading             = false;
            $scope.itinerariesDropdownList = response.data;

          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }
        $scope.listOfItineraries();

        $scope.listOfTrips = function(count)
        {
          $scope.dataLoading = true;
          $scope.promise = Trips.listFeaturedTripsRandom(count);
          $scope.promise.then(function(response)
          {
            $scope.tripsList  = response.data;
            $scope.dataLoading = false;
          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }

        $scope.listOfTrips(3);

        $scope.getTripDetails = function(trip)
        {
          $state.go('holiday', {tripId:trip.id});
          $rootScope.scrollTop();
        }

        $scope.listOfPartners = function()
        {
          $scope.dataLoading = true;
          $scope.promise = Partners.getShownPartners();
          $scope.promise.then(function(response)
          {
            $scope.dataLoading = false;
            $scope.partnersList = response.data;
            setTimeout(function() {
              $('.carousel-showmanymoveone .item').each(function(){
                var itemToClone = $(this);
                for (var i = 1; i < 6; i++) {
                  itemToClone = itemToClone.next();
                  // wrap around if at end of item collection
                  if (!itemToClone.length) {
                    itemToClone = $(this).siblings(':first');
                  }
                  // grab item, clone, add marker class, add to collection
                  itemToClone.children(':first-child').clone()
                  .addClass("cloneditem-"+(i))
                  .appendTo($(this)); }
                });
            }, 500);
          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }
        $scope.listOfPartners();

        $scope.listOfTabs = function()
        {
          $scope.promise = HomeTabs.listTabs();
          $scope.promise.then(function(response)
          {
            var tabsList = response.data;
            $scope.tabs  = {};
            for(var i = 0; i < tabsList.length; i++)
            {
              if(tabsList[i].tab_name == 'tours') $scope.tabs.tours = tabsList[i];
              else if(tabsList[i].tab_name == 'booking') $scope.tabs.booking = tabsList[i];
              else if(tabsList[i].tab_name == 'deals') $scope.tabs.deals = tabsList[i];
            }
            $scope.dataLoading = false;
          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }

        $scope.listOfTabs();

        $scope.searchText = {};
        $scope.search = function(searchText)
        {
          $rootScope.searchObj = searchText;
          $rootScope.homeFilter = true;
          $state.go('packages');
        }
        $scope.searchByCity = function(cityID) 
        {
          var searchText = {}
          searchText.city_id = cityID;
          $scope.search(searchText);
        }
        $scope.searchByCategory = function(categoryID) 
        {
          $state.go('categoriesPackages', { categoryId: categoryID })
        }
        
      }]);