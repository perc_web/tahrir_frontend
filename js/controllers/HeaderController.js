angular.module("TahrerApp").controller('HeaderController', ['$scope', '$rootScope', 'localStorageService', '$state', 'debug','loading_timeout','$window','$translate','Settings',
	function ($scope, $rootScope, localStorageService, $state, debug, loading_timeout, $window, $translate, Settings) {

		$scope.listOfSettings = function()
		{
			$scope.promise = Settings.listSettings();
			$scope.promise.then(function(response)
			{
				$rootScope.setting = response.data;
				$rootScope.packageImgs = response.data.package_page_images.value[Math.floor(Math.random()*$rootScope.setting.package_page_images.value.length)];

            }, function(error)
            {
            	
            });
		}
		$scope.listOfSettings();

	}]);
