angular.module("TahrerApp").controller('ContactUsController', ['$scope', '$rootScope', 'localStorageService', '$state', '$timeout', 'loading_timeout', 'debug', '$window', '$filter','Countries', 'Contact',
	function ($scope, $rootScope, localStorageService, $state, $timeout, loading_timeout, debug, $window, $filter, Countries, Contact) {

        $scope.listOfCountries = function()
        {
          $scope.dataLoading = true;
          $scope.promise = Countries.getAllCountries();
          $scope.promise.then(function(response)
          {
            $scope.dataLoading = false;
            $scope.countriesDropdownList = response.data;

          }, function(error)
          {
            $scope.dataLoading = false;
          });
        }

        $scope.listOfCountries();

        $scope.listCitiesByCountry = function(countryID)
        {
          $scope.dataTransparentLoading = true;
          $scope.promise = Countries.listCitiesByCountry(countryID);
          $scope.promise.then(function(response)
          {
            $scope.dataTransparentLoading = false;
            $scope.citiesDropdownList = response.data;

          }, function(error)
          {
            $scope.dataTransparentLoading = false;
          });
        }

		$scope.contact = {};
		$scope.submit = function() 
		{
			if($scope.contactForm.$invalid)
			{
				return;
			}

      $scope.contact.from    = $filter("date")($scope.contact.from, "yyyy-MM-dd HH:mm:ss");
      $scope.contact.to      = $filter("date")($scope.contact.to, "yyyy-MM-dd HH:mm:ss");
      $scope.contact.country = $scope.contact.country.name;
      $scope.contact.city    = $scope.contact.city.name;
      $scope.dataLoading  = true;
      $scope.promise = Contact.contactUs($scope.contact);
      $scope.promise.then(function()
      {
        $scope.contact = {};
        $scope.contactSuccess = true;
        $scope.dataLoading = false;

        setTimeout(function() 
        {
          $scope.contactSuccess = false;
        }, 1500);

      }, function(error)
      {
        $scope.contactSuccess = false;
        $scope.dataLoading = false;
      });
		}

	}]);