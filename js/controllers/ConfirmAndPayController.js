angular.module("TahrerApp").controller("ConfirmAndPayController", [
  "$scope",
  "$rootScope",
  "localStorageService",
  "$state",
  "$timeout",
  "loading_timeout",
  "debug",
  "$window",
  "payfortSrc",
  "$filter",
  function(
    $scope,
    $rootScope,
    localStorageService,
    $state,
    $timeout,
    loading_timeout,
    debug,
    $window,
    payfortSrc,
    $filter
  ) {
    $scope.$on("$destroy", function() {
      localStorageService.removeData("holidayData");
    });
    if (!localStorageService.getData("holidayData")) {
      $state.go("packages");
    }
    $scope.holiday = localStorageService.getData("holidayData");
    $scope.holiday.from = new Date($scope.holiday.from);
    $scope.holiday.total = new Number($scope.holiday.price).toFixed(1);
	$scope.holiday.price = parseFloat($scope.holiday.total) / 1.03;
    $scope.holiday.service = $scope.holiday.price * 0.03;
    $scope.holiday.service = new Number($scope.holiday.service).toFixed(1);
    $scope.payfortSrc = false;
    $scope.payfortSrc = payfortSrc + $scope.holiday.id + "/en";
    var frame = document.getElementById("frame");
    $scope.refreshFrame = function() {
      $("#frame").attr("src", $("#frame").attr("src"));
      $scope.frameLoaded = false;
    };

    frame.onload = function() {
      try {
        if (
          frame.contentWindow.location.href !== "about:blank" &&
          frame.contentWindow.location.href == $scope.payfortSrc
        ) {
          $scope.frameLoaded = false;
        }
      } catch (e) {
        $scope.frameLoaded = true;
        $scope.$apply();
      }
    };
  }
]);
