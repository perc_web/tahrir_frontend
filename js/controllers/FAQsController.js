angular.module("TahrerApp").controller('FAQsController', ['$scope', '$rootScope', 'localStorageService', '$state', '$timeout', 'loading_timeout', 'debug', '$window', '$filter','FAQs',
	function ($scope, $rootScope, localStorageService, $state, $timeout, loading_timeout, debug, $window, $filter, FAQs) {

		$scope.getAllFaqs = function()
		{
			$scope.dataLoading = true;
			$scope.promise = FAQs.getAllFaqs();
			$scope.promise.then(function(response)
			{
				$scope.dataLoading = false;
				$scope.faqsList = response.data;

			}, function(error)
			{
				$scope.dataLoading = false;
			});
		}

		$scope.getAllFaqs();
		
	}]);