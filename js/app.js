
var TahrerApp = angular.module("TahrerApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "smart-table",
    "ngAnimate",
    "pascalprecht.translate",
    "ngFileUpload",
    'checklist-model',
    "ngMaterial"

]);


TahrerApp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
    });
}]);



TahrerApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/home");

    $stateProvider
        .state('home', {
            url: "/home",
            cashe:false,
            templateUrl: "views/partials/home.html",
            data: {pageTitle: 'Tahrir Tours'},
            controller: "HomeController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js',
                        'js/controllers/HomeController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('packages', {
            url: "/packages",
            cashe:false,
            templateUrl: "views/partials/packages.html",
            data: {pageTitle: 'Packages'},
            controller: "PackagesController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js', 
                        'js/controllers/PackagesController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('categoriesPackages', {
            url: "/packages/:categoryId",
            cashe:false,
            templateUrl: "views/partials/packages.html",
            data: {pageTitle: 'Packages'},
            controller: "PackagesController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js', 
                        'js/controllers/PackagesController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('holiday', {
            url: "/holiday/:tripId",
            cashe:false,
            templateUrl: "views/partials/holiday.html",
            data: {pageTitle: 'Holiday'},
            controller: "HolidayController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js',
                        'js/controllers/HolidayController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('contactUs', {
            url: "/contactUs",
            cashe:false,
            templateUrl: "views/partials/contactUs.html",
            data: {pageTitle: 'Contact Us'},
            controller: "ContactUsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js',
                        'js/controllers/ContactUsController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('aboutUs', {
            url: "/aboutUs",
            templateUrl: "views/partials/aboutUs.html",
            data: {pageTitle: 'About Us'},
            controller: "AboutUsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js',
                        'js/controllers/AboutUsController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('FAQs', {
            url: "/FAQs",
            templateUrl: "views/partials/FAQs.html",
            data: {pageTitle: 'FAQs'},
            controller: "FAQsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js',
                        'js/controllers/FAQsController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('terms', {
            url: "/terms",
            templateUrl: "views/partials/terms.html",
            data: {pageTitle: 'Terms and conditions'},
            controller: "TermsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js',
                        'js/controllers/TermsController.js'
                        ]
                    });
                }]
            }
        })
    $stateProvider
        .state('confirmAndPay', {
            url: "/confirmAndPay",
            cashe:false,
            templateUrl: "views/partials/confirmAndPay.html",
            data: {pageTitle: 'Confirm And Pay'},
            controller: "ConfirmAndPayController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'TahrerApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        'assets/global/plugins/select2/css/select2.min.css',
                        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        'assets/global/plugins/select2/js/select2.full.min.js',
                        'js/controllers/ConfirmAndPayController.js'
                        ]
                    });
                }]
            }
        })

                        
}]);

TahrerApp.run([ "$rootScope", "$state", "$location", "localStorageService", "$translate","$timeout", 
    function ($rootScope,  $state, $location, localStorageService, $translate, $timeout) {
    $rootScope.$state = $state; 
    $rootScope.location = $location;
    $rootScope.refresh = function()
    {
        location.reload();
    }
    $rootScope.selectLang = function (lang) 
    {
        if(lang == null)
            $rootScope.lang = 'us';
        else
            $rootScope.lang = lang;
    }
    $rootScope.selectLang(localStorageService.getData('TahrirFrontlang')); 

    $rootScope.$watch('lang', function (newValue, oldValue, scope) {
        localStorageService.setData('TahrirFrontlang',newValue);
        $translate.use(newValue);
        if (newValue != oldValue) $rootScope.refresh();
    });

    $rootScope.scrollTop = function()
    {
        var scrollTop = $(".scrollTop");
        $('html, body').animate({
          scrollTop: 0}, 0);
    }
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams)
        {
            $rootScope.scrollTop();
            if(toState.name != 'packages')
            {
                delete $rootScope.searchObj;
            }
            $(document).ready(function(){
                $timeout(function()
                {
                    $(".nav#mainNav li.active").removeClass("active");
                    if(toState.name =='holiday') {
                        $(".nav#mainNav li#packages").addClass("active");
                    }
                    else {
                        $(".nav#mainNav li#" + toState.name).addClass("active");
                    }
                }, 0);
            });   
        });
    }]);

