
var translationsUS = {
    LanguageLabel: 'English',
    Home:"Home",
    Packages:"Packages",
    About:"About",
    FAQs:"FAQs",
    Contact_Us:"Contact Us",
    ABOUT_US:"ABOUT US",
    Country:"Country",
    City:"City",
    Choose_destination:"Choose destination",
    Choose_Country:"Choose Country",
    Search_what_you_like:"Search what you like",
    Keyword:"Keyword",
    Check_in:"Check in",
    Check_out:"Check out",
    Search:"Search",
    Trending:"Trending",
    Duration:"Duration",
    Type:"Type",
    Categories:"Categories",
    Price:"Price",
    Price_Range:"Price Range",
    Browse:"Browse",
    Why_us:"Why us?",
    Tahrir_Tours:"Tahrir Tours",
    How_to_book:"How to book",
    Crazy_Deals:"Crazy Deals",
    Our_Partners:"Our Partners",
    send_it_now:"Send It Now",
    Contact_Us_Success:"Your Message has been sent",
    Enquire_Us_Success:"Your Enquire has been sent",
    load_more:"Load More",
    from:"from",
    From:"From",
    To:"To",
    Location:"Location",
    Run:"Run",
    Overview:"Overview",
    Itinerary:"Itinerary",
    Accommodation:"Accommodation",
    ENQUIRE_NOW:"ENQUIRE NOW",
    Price_Per_Person:"Price Per Person",
    Price_Per_vehicle:"Price Per vehicle",
    Included:"Included",
    Excluded:"Excluded",
    Person:"Person",
    Persons:"Persons",
    DAY:"DAY",
    Accommodation_Plan:"Accommodation Plan",
    CHECK_AVAILABILITY:"CHECK AVAILABILITY",
    Name:"Name",
    Email:"E-mail",
    Phone:"Phone",
    No_OF_Adults:"No. of Adults",
    No_OF_Childs:"No. of Childs",
    years:"years",
    Please_advise_your_requirements:"Please advise your requirements",
    Submit:"Submit",
    You_Might_Also_Like:"You Might Also Like",
    children_policy:"Children Policy",
    cancellation_policy:"Cancellation Policy",
    tour_voucher:"Tour Voucher",
    payment:"Payment",
    Footer_MSG:"2017-2018 Tahrir Tours. All Rights Reserved.",
    requierd_fields_indicator:"Required Fields indicated with",
    customer_support:"Didn't find your answer? call our customer support team",
    Contact_Us_Now:"Contact us now ",
    Customise_your_trip:"Customise your trip",
    filter_by:"Filter By",
    TOTAL:"TOTAL",
    Nationality :"Nationality",
    Choose_Nationality :"Choose Nationality",
    Tour_Language :"Tour Language",
    Choose_Tour_Language :"Choose Tour Language",
    enquire_msg :"You won't be charged yet",
    Baby_Chair:"Baby Chair",
    pick_off:"Pick Up",
    drop_off:"Drop Off",
    No_OF_PAX:"No Of Pax",
    Choose_PACKAGE:"Choose Package",
    Starting_From:"Starting From",
    notes:"Notes",
    Duration:"Duration",
    Days:"Days",
    Day:"Day",
    flight_number:"Flight number",
    airline:"airline",

};

var translationsFR = {
    LanguageLabel: 'French',

};

var translationsSP = {
    LanguageLabel: 'Spanich',

};


angular.module("TahrerApp").config(['$translateProvider', function ($translateProvider) {
    // add translation tables
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
    $translateProvider.translations('us', translationsUS);
    $translateProvider.translations('fr', translationsFR);
    $translateProvider.translations('sp', translationsSP);
    $translateProvider.preferredLanguage('us');
    $translateProvider.fallbackLanguage('us');

}]);