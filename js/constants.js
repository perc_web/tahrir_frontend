 /*
 * lOCALHOST  server URL
 */
var time = (new Date().getTimezoneOffset() / 60) * -1;


// angular.module('TahrerApp').constant('serverip', 'http://192.168.1.4/tahrir-backend/api/v1/');
// angular.module('TahrerApp').constant('payfortSrc', 'http://192.168.1.4/tahrir-backend/payfort/redirect/');

angular.module('TahrerApp').constant('payfortSrc', 'http://51.15.132.57/tahrir-backend/payfort/redirect/');
angular.module('TahrerApp').constant('serverip', 'http://51.15.132.57/tahrir-backend/api/v1/');


/**
 * Headers for calling rest
 */
angular.module('TahrerApp').constant('HEADERS',
{
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'time-zone-diff': time,
    'locale':'en'
});
angular.module('TahrerApp').constant('loading_timeout', 1000);
angular.module('TahrerApp').value('debug', false);


